package org.bitbucket.jizou;

import java.io.Serializable;

public class Context implements Serializable {
    private static final long serialVersionUID = 7156176142797383071L;

    private String dir = "/tmp";
    private int port = 8888;
    private boolean running = false;
    private boolean runOnStartup = false;

    public Context(){
    }

    public Context(Context self){
        this.runOnStartup = self.runOnStartup;
        this.dir = self.dir;
        this.port = self.port;
        this.running = self.running;
    }

    public String getDirectory(){
        return dir;
    }

    public int getPort(){
        return port;
    }

    public boolean isRunning(){
        return running;
    }

    public boolean isRunOnStartup(){
        return runOnStartup;
    }

    public void setDirectory(String dir){
        this.dir = dir;
    }

    public void setPort(int port){
        this.port = port;
    }

    public void setRunning(boolean running){
        this.running = running;
    }

    public void setRunOnStartup(boolean runOnStartup){
        this.runOnStartup = runOnStartup;
    }

    public String toString(){
        return String.format(
            "context{ dir: %s, port: %d, runnnig: %s }",
            getDirectory(), getPort(), isRunning()
        );
    }
}