package org.bitbucket.jizou;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import winstone.Launcher;

public class JizouObject{
    private Launcher launcher;
    private URL warLocation;

    public JizouObject(){
        // war 自身の場所を取得
        warLocation = Main.class.getProtectionDomain().getCodeSource().getLocation();
    }

    public boolean isRunning(){
        return launcher != null && launcher.isRunning();
    }

    public void reload(Context context) throws IOException{
        stop(context);
        start(context);
    }

    public void start(Context context) throws IOException{
        // コマンドライン引数ほぼそのまま winstone に渡す。
        // war ファイルの場所だけ加える。
        Map<String, String> props = new HashMap<>();
        props.put("httpPort", Integer.toString(context.getPort()));
        props.put("useJasper", "true");
        props.put("warfile", warLocation.getPath());
        System.setProperty("jizou.root", context.getDirectory());

        launcher = new Launcher(props);
        while(!isRunning()){
            try{
                Thread.sleep(100);
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        context.setRunning(isRunning());
    }

    public void stop(Context context){
        if(isRunning()){
            launcher.shutdown();
            launcher = null;

            context.setRunning(isRunning());
        }
    }
}