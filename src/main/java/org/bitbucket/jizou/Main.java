package org.bitbucket.jizou;

import org.bitbucket.jizou.gui.JizouSystem;

public class Main{
    public Main(String[] args) throws Exception{
        JizouSystem system = new JizouSystem();
        system.start();
    }

    public static void main(String[] args) throws Exception{
        new Main(args);
    }
}