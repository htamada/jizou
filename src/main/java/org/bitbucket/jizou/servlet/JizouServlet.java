package org.bitbucket.jizou.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import net.sf.webdav.IWebdavStore;
import net.sf.webdav.WebdavServlet;

public class JizouServlet extends WebdavServlet{
    private static final long serialVersionUID = 8895894599122787313L;

    private Map<String, String> props;

    /**
     * Almost routine is copied from the super class.
     * reset root directory is own routine.
     */
    @Override
    protected IWebdavStore constructStore(String clazzName, File root) {
        IWebdavStore webdavStore;
        try {
            Class<?> clazz = WebdavServlet.class.getClassLoader().loadClass(clazzName);

            Constructor<?> ctor = clazz.getConstructor(new Class[] { File.class });

            root = getFileRoot(root);

            webdavStore = (IWebdavStore) ctor.newInstance(new Object[] { root });
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("some problem making store component", e);
        }
        return webdavStore;
    }

    protected File getFileRoot(){
        String path = System.getProperty("jizou.root");
        System.out.printf("jizou.root: %s%n", path);
        if(path == null){
            path = props.get("jizou.root");
        }
        if(path == null){
            return null;
        }
        return new File(path);
    }

    protected File getFileRoot(File defaultRoot){
        File root = getFileRoot();
        if(root == null){
            root = defaultRoot;
        }
        return root;
    }

    @Override
    public void init() throws ServletException{
        super.init();

        File propFile = new File(System.getProperty("user.home"), ".jizou.properties");
        if(propFile.exists()){
            this.props = readProps(propFile);
        }
    }

    private Map<String, String> readProps(File propFile) throws ServletException{
        Map<String, String> props = new HashMap<>();
        try(BufferedReader in = new BufferedReader(
                new InputStreamReader(new FileInputStream(propFile), "utf-8"))){
            String line;
            while((line = in.readLine()) != null){
                line = line.trim();
                if(line.startsWith("#")){
                    continue;
                }
                int index = line.indexOf('=');
                if(index >= 0){
                    props.put(line.substring(0, index), line.substring(index + 1));
                }
            }
        } catch(IOException e){
            throw new ServletException(e);
        }
        return props;
    }
}
