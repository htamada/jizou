package org.bitbucket.jizou.gui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.bitbucket.jizou.Context;

public class AboutPane implements PropertyChangeListener{
    private Icon icon;
    private JPanel panel;
    private JLabel path;
    private JLabel port;
    private JLabel runOnStartup;
    private JLabel status;

    public AboutPane(Context context){
        initObject(context);
        initLayout();
    }

    public JComponent getComponent(){
        return panel;
    }

    public Icon getIcon(){
        return icon;
    }

    private void initLayout(){
        GridBagConstraints gbc = new GridBagConstraints();
        GridBagLayout layout = new GridBagLayout();
        panel.setLayout(layout);

        JLabel about = new JLabel(readFrom("/resources/about.html"));
        JLabel label1 = new JLabel("Status");
        JLabel label2 = new JLabel("Directory");
        JLabel label3 = new JLabel("Port");
        JLabel label4 = new JLabel("Run on startup");

        gbc.gridwidth = 2;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(about, gbc);
        layout.setConstraints(about, gbc);

        gbc.anchor = GridBagConstraints.WEST;
        gbc.gridwidth = 1;
        gbc.gridy = 1;
        gbc.gridx = 0;
        panel.add(label1, gbc);
        layout.setConstraints(label1, gbc);
        gbc.gridx = 1;
        panel.add(status, gbc);
        layout.setConstraints(status, gbc);

        gbc.gridy = 2;
        gbc.gridx = 0;
        panel.add(label2, gbc);
        layout.setConstraints(label2, gbc);
        gbc.gridx = 1;
        panel.add(path, gbc);
        layout.setConstraints(path, gbc);

        gbc.gridy = 3;
        gbc.gridx = 0;
        panel.add(label3, gbc);
        layout.setConstraints(label3, gbc);
        gbc.gridx = 1;
        panel.add(port, gbc);
        layout.setConstraints(port, gbc);

        gbc.gridy = 4;
        gbc.gridx = 0;
        panel.add(label4, gbc);
        layout.setConstraints(label4, gbc);
        gbc.gridx = 1;
        panel.add(runOnStartup, gbc);
        layout.setConstraints(runOnStartup, gbc);

        gbc.gridwidth = 2;
        gbc.gridy = 5;
        gbc.gridx = 0;
        Component box = Box.createHorizontalStrut(300);
        panel.add(box, gbc);
        layout.setConstraints(box, gbc);
    }

    private void initObject(Context context){
        status = new JLabel("" + context.isRunning(), JLabel.LEFT);
        port = new JLabel("" + context.getPort(), JLabel.LEFT);
        path = new JLabel(context.getDirectory(), JLabel.LEFT);
        runOnStartup = new JLabel("" + context.isRunOnStartup(), JLabel.LEFT);

        icon = new ImageIcon(getClass().getResource("/resources/jizou_small.png"));

        updateValues(context);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        Context context = (Context)evt.getNewValue();
        updateValues(context);
    }

    private String readFrom(String resourcePath){
        StringBuilder sb = new StringBuilder();
    
        try(BufferedReader in = new BufferedReader(
                new InputStreamReader(getClass().getResourceAsStream(resourcePath), "utf-8"))){
            String line;
            while((line = in.readLine()) != null){
                sb.append(line);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
        return new String(sb);
    }

    private void updateValues(Context context){
        panel = new JPanel();
        status.setText("" + context.isRunning());
        port.setText("" + context.getPort());
        path.setText(context.getDirectory());
        runOnStartup.setText("" + context.isRunOnStartup());
    }
}
