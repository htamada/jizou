package org.bitbucket.jizou.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import org.bitbucket.jizou.Context;

public class SettingPanel{
    private Context context = new Context();
    private JTextField directory;
    private List<PropertyChangeListener> listeners = new ArrayList<>();
    private SpinnerNumberModel model; 
    private JPanel panel;

    private JSpinner portSpinner;
    private JCheckBox runOnStartup;

    public SettingPanel(){
        initialize();
        setContext(context);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener){
        listeners.add(listener);
    }

    protected synchronized void fireEvent(PropertyChangeEvent event){
        for(PropertyChangeListener listener: listeners){
            listener.propertyChange(event);
        }
    }

    public Context getContext(){
        updateValues();
        return context;
    }

    public JComponent getPanel(){
        return panel;
    }

    private void initialize(){
        initInstance();
        initLayout();
    }

    private void initInstance(){
        panel = new JPanel();
        directory = new JTextField(20);
        portSpinner = new JSpinner(model = new SpinnerNumberModel(
            8888, 256, Integer.MAX_VALUE, 1
        ));
        runOnStartup = new JCheckBox();
        runOnStartup.setEnabled(false);
    }

    private void initLayout(){
        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();

        JLabel label1 = new JLabel("Base Directory");
        JLabel label2 = new JLabel("Port number");
        JLabel label3 = new JLabel("Run on startup");

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.anchor = GridBagConstraints.WEST;
        gbl.setConstraints(label1, gbc);
        gbc.gridx = 1;
        gbl.setConstraints(directory, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbl.setConstraints(label2, gbc);
        gbc.gridx = 1;
        gbl.setConstraints(portSpinner, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbl.setConstraints(label3, gbc);
        gbc.gridx = 1;
        gbl.setConstraints(runOnStartup, gbc);

        panel.add(label1);
        panel.add(label2);
        panel.add(label3);
        panel.add(directory);
        panel.add(portSpinner);
        // panel.add(runOnStartup);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener){
        listeners.remove(listener);
    }

    void resetEditor(){
        setValues(context);
    }

    public void setContext(Context context){
        this.context = context;
        setValues(context);
    }

    private void setValues(Context context){
        directory.setText(context.getDirectory());
        model.setValue(context.getPort());
        runOnStartup.setSelected(context.isRunOnStartup());
    }

    private void updateValues(){
        Context oldValue = new Context(context);
        context.setDirectory(directory.getText());
        context.setPort(model.getNumber().intValue());
        context.setRunOnStartup(runOnStartup.isSelected());

        fireEvent(new PropertyChangeEvent(this, "context", oldValue, context));
    }
}