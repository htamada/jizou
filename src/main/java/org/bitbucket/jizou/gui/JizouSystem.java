package org.bitbucket.jizou.gui;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.bitbucket.jizou.Context;
import org.bitbucket.jizou.JizouObject;

public class JizouSystem{
    private AboutPane aboutPane;
    private TrayIcon icon;
    private JizouObject object;
    private SettingPanel panel;
    
    private MenuItem start, stop, config;

    public JizouSystem(){
        object = new JizouObject();
        panel = new SettingPanel();

        initSystemTray();
    }

    private MenuItem buildMenuItem(String label, ActionListener listener){
        MenuItem item = new MenuItem(label);
        item.addActionListener(listener);

        return item;
    }

    private PopupMenu buildPopupMenu(){
        PopupMenu menu = new PopupMenu();

        MenuItem about = buildMenuItem("About...", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                showAbout();
            }
        });
        start = buildMenuItem("Start", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                start();
            }
        });
        stop = buildMenuItem("Stop", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                stop();
            }
        });
        config = buildMenuItem("Configuration...", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                config();
            }
        });
        MenuItem quit = buildMenuItem("Quit", new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                quit();
            }
        });
        menu.add(about);
        menu.addSeparator();
        menu.add(start);
        menu.addSeparator();
        menu.add(stop);
        menu.addSeparator();
        menu.add(config);
        menu.addSeparator();
        menu.add(quit);

        return menu;
    }

    private TrayIcon buildTrayIcon(){
        ImageIcon imageIcon = new ImageIcon(getClass().getResource("/resources/jizou.png"));
        final TrayIcon icon = new TrayIcon(imageIcon.getImage(), "Webdav server application");
        icon.setImageAutoSize(true);
        icon.setPopupMenu(buildPopupMenu());

        return icon;
    }

    private void config(){
        int option = JOptionPane.showConfirmDialog(
            null, panel.getPanel(), "Configuration", JOptionPane.OK_CANCEL_OPTION
        );

        if(option == JOptionPane.OK_OPTION){
            String message = "start server?";
            if(object.isRunning()){
                message = "restart server?";
            }
            if(confirm(message)){
                reload();
            }
        }
        else{
            panel.resetEditor();
        }
    }

    private boolean confirm(String message){
        int yesOrNo = JOptionPane.showConfirmDialog(
            null, message, "Yes or No", JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE
        );
        return yesOrNo == JOptionPane.YES_OPTION;
    }

    private void contextUpdated(Context context){
        boolean running = context.isRunning();
        start.setEnabled(!running);
        config.setEnabled(!running);
        stop.setEnabled(running);

        panel.fireEvent(new PropertyChangeEvent(this, "context", null, context));
    }

    private void initDefault(){
    }

    private void initSystemTray(){
        if(!SystemTray.isSupported()){
            initDefault();
        }

        try{
            SystemTray tray = SystemTray.getSystemTray();
            icon = buildTrayIcon();
            tray.add(icon);
        } catch(AWTException e){
            e.printStackTrace();
        }

        contextUpdated(panel.getContext());
    }

    public void quit(){
        if(confirm("Quit the server?")){
            stop();

            SystemTray.getSystemTray().remove(icon);
        }
    }

    public void reload(){
        try{
            Context context = panel.getContext();
            object.reload(context);

            contextUpdated(context);
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private void showAbout(){
        if(aboutPane == null){
            aboutPane = new AboutPane(panel.getContext());
            panel.addPropertyChangeListener(aboutPane);
        }
        JOptionPane.showMessageDialog(
            null, aboutPane.getComponent(), "About Jizou",
            JOptionPane.INFORMATION_MESSAGE, aboutPane.getIcon()
        );
    }

    public void start(){
        try{
            Context context = panel.getContext();
            object.start(context);

            contextUpdated(context);
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public void stop(){
        Context context = panel.getContext();
        object.stop(context);

        contextUpdated(context);
    }
}