package org.bitbucket.jizou;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.bitbucket.jizou.Context;
import org.junit.Before;
import org.junit.Test;

public class ContextTest {
    private Context context;

    @Before
    public void setUp(){
        context = new Context();
    }

    @Test
    public void testDefaultValue(){
        assertThat(context.getDirectory(), is("/tmp"));
        assertThat(context.getPort(), is(8888));
        assertThat(context.isRunning(), is(false));
        assertThat(context.isRunOnStartup(), is(false));
    }

    @Test
    public void testSetter(){
        context.setDirectory("/Users/tamada");
        context.setPort(8080);
        context.setRunning(true);
        context.setRunOnStartup(true);

        assertThat(context.getDirectory(), is("/Users/tamada"));
        assertThat(context.getPort(), is(8080));
        assertThat(context.isRunning(), is(true));
        assertThat(context.isRunOnStartup(), is(true));
    }

    @Test
    public void testSelfConstructor(){
        context.setDirectory("/Users/tamada");
        context.setPort(8080);
        context.setRunning(true);
        context.setRunOnStartup(true);

        Context newContext = new Context(context);

        assertThat(newContext.getDirectory(), is("/Users/tamada"));
        assertThat(newContext.getPort(), is(8080));
        assertThat(newContext.isRunning(), is(true));
        assertThat(newContext.isRunOnStartup(), is(true));
    }
}
