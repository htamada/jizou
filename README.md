JIZOU 1.0
=========

Overview
--------

This product is simplest webdav server.
No need to install, and to edit config file.
To run the product, simply double click.

Install
-------

Simply run ```mvn package```, then ```jizou-1.0.war``` is built.

For OS X, run ```mvn assembly:assembly```, then ```jizou-1.0.app``` is bundled.

Usage
-----

Double click war file, or app file, then launch the server.

If servlet container is already running on your environment,
put ```jizou-1.0.war``` on ```webapp``` directory of your servlet container.

Then, access ```http://localhost:8888``` by some webdav client
application, such as Finder (OS X), and Explorer (Windows).
Note that, replace port number on your environment, if JIZOU is running on your servlet container.

Requirements
------------

* Runtime/Development Environment
    * Java SE 7
    * [Winstone](http://winstone.sourceforge.net)
* Dependency
    * [Webdav-servlet](https://github.com/ceefour/webdav-servlet)
* Project Management
    * [Maven 3.x](http://maven.apache.org/)
* Test Environment
    * [JUnit 4.12](http://www.junit.org/)

Authors
-------

* Author
    * Haruaki Tamada
* Web page
    * http://htamada.bitbucket.org/jizou/

License
-------

Apache License Version 2.0
    Copyright 2013- Haruaki Tamada

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
